# Sorted Linked List

The `SortedLinkedList` is a PHP implementation of a singly linked list that maintains its elements in a sorted order. This list supports insertion of integers and strings. However, a single list instance can contain only one data type, either integers or strings, not both.

## Requirements

* PHP 8.2 or higher

## Tests

This library boasts a 100% code coverage, ensuring reliability and stability.
To run the tests, you need to install the dependencies via Composer.

then run the following command:

```bash
vendor/bin/phpunit --coverage-text .SortedLinkedList/tests/.
```

## Installation

Use [Composer](https://getcomposer.org/) to install the package.

```bash
composer require maksymilian.kowalewski/sorted-linked-list
```

## Usage

Below quick guide how to use SortedLinkedList. Keep in mind that once you insert an integer, you cannot insert a string and vice versa. If you try to insert a different data type after inserting the first item, it will throw an InvalidArgumentException.

### Initialization

1. First add the `use` statement.
```php
use SortedLinkedList\SortedLinkedList;

$list = new SortedLinkedList();
```

2. Add elements to the list.
```php
$list->insert(5);
$list->insert(2);
$list->insert(10);
```

3. Then you would have two options - you can retrive first element or full list in sorted order.

To get first:
```php
$firstValue = $list->getFirst();
```
To get full list:
```php
$allValues = $list->getAll();
```

## What's Under the Hood?
The SortedLinkedList uses a private Node class to store each item in the list. The list is always sorted in ascending order during insertion. Here are some of its methods:

* insert(int|string $value): void: Inserts a new value into the list.
* getFirst(): int|string|null: Retrieves the first value from the list.
* getAll(): array: Retrieves all values from the list.

The class ensures type consistency, meaning you can't mix integers and strings in a single list instance.

## Contribution
If you find any issues or have suggestions, feel free to open an issue or create a pull request.