<?php

namespace SortedLinkedList;

class Node
{
    /**
     * @var string|int
     */
    public string|int $value;
    /**
     * @var null | Node
     */
    public ?Node $next;

    /**
     * Node constructor.
     * @param int|string $value
     */
    public function __construct(int|string $value)
    {
        $this->value = $value;
        $this->next = null;
    }
}