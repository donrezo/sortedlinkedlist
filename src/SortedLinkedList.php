<?php

namespace SortedLinkedList;

use InvalidArgumentException;

class SortedLinkedList
{
    /** @var ?Node */
    private ?Node $head;

    /** @var ?string */
    private ?string $type;

    public function __construct()
    {
        $this->head = null;
        $this->type = null;
    }

    /**
     * Inserts a new value into the sorted linked list.
     *
     * @param int|string $value
     * @return void
     */
    public function insert(int|string $value): void
    {
        $this->ensureCorrectType($value);

        $newNode = new Node($value);
        $this->insertNode($newNode);
    }

    /**
     * Inserts a new node into the sorted linked list.
     *
     * @param Node $newNode
     * @return void
     */
    private function insertNode(Node $newNode): void
    {
        if ($this->head === null || $this->compare($newNode->value, $this->head->value) < 0) {
            $newNode->next = $this->head;
            $this->head = $newNode;
        } else {
            $current = $this->head;
            while ($current->next !== null && $this->compare($newNode->value, $current->next->value) > 0) {
                $current = $current->next;
            }
            $newNode->next = $current->next;
            $current->next = $newNode;
        }
    }

    /**
     * Check type of value if its either string or int and compare it with type of list.
     *
     * @param int|string $value
     * @return void
     */
    private function ensureCorrectType(int|string $value): void
    {
        if ($this->type === null) {
            $this->type = is_int($value) ? 'int' : 'string';
            return;
        }

        if (($this->type === 'int' && !is_int($value)) || ($this->type === 'string' && !is_string($value))) {
            throw new InvalidArgumentException('Cannot insert mixed data types into the list.');
        }
    }

    /**
     * Retrieves the first value from the sorted linked list.
     *
     * @return int|string|null
     */
    public function getFirst(): int|string|null
    {
        return $this->head?->value;
    }

    /**
     * Retrieves all values from the sorted linked list.
     *
     * @return list<int|string>
     */
    public function getAll(): array
    {
        $values = [];
        $current = $this->head;

        while ($current !== null) {
            $values[] = $current->value;
            $current = $current->next;
        }

        return $values;
    }

    /**
     * Compares two values based on the type of the list.
     *
     * @param int|string $a
     * @param int|string $b
     * @return int
     */
    private function compare(int|string $a, int|string $b): int
    {
        if ($this->type === 'int') {
            return $a - $b;
        } else {
            return strcmp($a, $b);
        }
    }
}