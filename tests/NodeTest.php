<?php

namespace SortedLinkedList\Tests;

use PHPUnit\Framework\TestCase;
use SortedLinkedList\Node;

class NodeTest extends TestCase
{
    /** @var Node */
    private Node $node;

    protected function setUp(): void
    {
        parent::setUp();
        $this->node = new Node(10);
    }

    protected function tearDown(): void
    {
        unset($this->node);
        parent::tearDown();
    }

    public function testCanCreateNodeWithIntegerValue(): void
    {
        $this->assertInstanceOf(Node::class, $this->node);
        $this->assertEquals(10, $this->node->value);
        $this->assertNull($this->node->next);
    }

    public function testCanCreateNodeWithStringValue(): void
    {
        $this->node = new Node('hello');

        $this->assertInstanceOf(Node::class, $this->node);
        $this->assertEquals('hello', $this->node->value);
        $this->assertNull($this->node->next);
    }

    public function testCanAssignNextNode(): void
    {
        $node2 = new Node(20);

        $this->node->next = $node2;

        $this->assertInstanceOf(Node::class, $this->node->next);
        $this->assertEquals($node2->value, $this->node->next->value);
    }

    public function testNextNodeDefaultsToNull(): void
    {
        $this->assertNull($this->node->next);
    }
}