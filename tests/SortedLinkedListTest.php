<?php

namespace SortedLinkedList\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use SortedLinkedList\SortedLinkedList;

class SortedLinkedListTest extends TestCase
{
    /**
     * @var SortedLinkedList|null
     */
    private ?SortedLinkedList $list;

    protected function setUp(): void
    {
        $this->list = new SortedLinkedList();
    }

    protected function tearDown(): void
    {
        $this->list = null;
    }

    public function testCanInsertIntValues(): void
    {
        $this->list->insert(5);
        $this->assertEquals(5, $this->list->getFirst());

        $this->list->insert(3);
        $this->assertEquals(3, $this->list->getFirst());

        $this->list->insert(10);
        $this->list->insert(1);
        $this->assertEquals(1, $this->list->getFirst());
    }

    public function testCanInsertStringValues(): void
    {
        $this->list->insert('dog');
        $this->assertEquals('dog', $this->list->getFirst());

        $this->list->insert('apple');
        $this->assertEquals('apple', $this->list->getFirst());

        $this->list->insert('zebra');
        $this->list->insert('cat');
        $this->assertEquals('apple', $this->list->getFirst());
    }

    public function testCannotInsertBothIntAndString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->list->insert(5);
        $this->list->insert('apple');
    }

    public function testGetFirstReturnsNullOnEmptyList(): void
    {
        $this->assertNull($this->list->getFirst());
    }

    public function testCanRetrieveAllValuesInSortedOrder(): void
    {
        $this->list->insert(5);
        $this->list->insert(10);
        $this->list->insert(2);
        $this->list->insert(6);

        $this->assertEquals([2, 5, 6, 10], $this->list->getAll());

        $this->list = new SortedLinkedList();
        $this->list->insert('apple');
        $this->list->insert('dog');
        $this->list->insert('cat');
        $this->list->insert('banana');

        $this->assertEquals(['apple', 'banana', 'cat', 'dog'], $this->list->getAll());
    }
}